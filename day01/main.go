package main

import (
	"fmt"
	"io/ioutil"
	"math"
	"strconv"
	"strings"
)

func CalcReqFuel(mass float64) int {
	return int(math.Floor(mass/3) - 2)
}

func CalcFuelRecursive(mass float64) int {
	if (int(math.Floor(mass/3) - 2)) <= 0 {
		return 0
	}
	return int(math.Floor(mass/3)-2) + CalcFuelRecursive((mass/3)-2)
}

func SumReqFuelPart1(strArr []string, sum_of_fuel int) {
	for _, i := range strArr {
		if x, err := strconv.ParseFloat(i, 64); err == nil {
			// fmt.Println("=> required fuel for mass", i, "is", CalcReqFuel(x))
			sum_of_fuel += CalcReqFuel(x)
		}
	}
	fmt.Println(sum_of_fuel)
}

func SumReqFuelPart2(strArr []string, sum_of_fuel int, sum_of_fuel_for_fuel int) {
	for _, i := range strArr {
		if x, err := strconv.ParseFloat(i, 64); err == nil {
			fuel_for_mass := CalcFuelRecursive(x)
			// fmt.Println("=> required fuel for mass", i, "is", fuel_for_mass)
			sum_of_fuel += fuel_for_mass
		}
	}
	fmt.Println("sum of fuel", sum_of_fuel)
}

func main() {
	fileName := "input.txt"
	// fileName := "sample.txt"
	input, err := ioutil.ReadFile(fileName)
	sum_of_fuel := 0
	sum_of_fuel_for_fuel := 0
	if err != nil {
		fmt.Println(err)
	}

	str := string(input) // convert byte to string, the reason of using string is to make use of the str characteristic -> \n to split the str into array

	// fmt.Println(str)

	strArr := strings.Split(str, "\n")
	// fmt.Println(len(strArr))

	// SumReqFuelPart1(strArr, sum)
	SumReqFuelPart2(strArr, sum_of_fuel, sum_of_fuel_for_fuel)
}
