# Calc the required fuel to launch

**Example**
1. Mass of 12, div by 3 = 4, round down -> 4 then minus 2 -> 2
2. Mass 0f 1969 -> required 654 gallon of fuel


therefore the eqn:

fuel = round_down(mass/3) - 2