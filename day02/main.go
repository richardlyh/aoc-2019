package main

import (
	"fmt"
	"io/ioutil"
	"strconv"
	"strings"
)

// func part1_sample() {
// 	// fileName := "sample.txt"
// 	fileName := "input.txt"
// 	input, err := ioutil.ReadFile(fileName)
// 	if err != nil {
// 		fmt.Println(err)
// 	}
// 	str := string(input)
// 	// fmt.Println(str) // >> debug
// 	a := strings.Split(str, ",")
// 	fmt.Println(a)                       // >> debug
// 	length := len(a)                     // >> debug
// 	fmt.Println("length of arr", length) // >> debug
// 	var arr []int64
// 	for _, i := range a {
// 		if x, err := strconv.ParseInt(i, 0, 64); err == nil {
// 			// fmt.Println("i: ", i, "x: ", x)
// 			arr = append(arr, x)
// 		}
// 	}
// 	fmt.Println("array: ", arr, "\n") // >> debug
// 	for i := 0; i < len(arr); i += 4 {
// 		operator := arr[i]
// 		number_a := arr[i+1]
// 		number_b := arr[i+2]
// 		var num_c int64
// 		if (i + 3) <= len(arr) {
// 			num_c = arr[i+3]
// 		}

// 		fmt.Println("operator: ", operator, "number_a: ", number_a, "number_b: ", number_b, "num_c: ", num_c) // >> debug
// 		if operator == 99 {
// 			fmt.Println(arr)
// 		} else if operator == 1 {
// 			arr[num_c] = arr[number_a] + arr[number_b]
// 			fmt.Println("operator ==> ", operator, " ", arr, " position ", num_c)
// 		} else if operator == 2 {
// 			arr[num_c] = arr[number_a] * arr[number_b]
// 			fmt.Println("operator ==> ", operator, " ", arr, " position ", num_c)
// 		}
// 	}
// }

func part_1() {
	fileName := "input.txt"
	input, err := ioutil.ReadFile(fileName)
	if err != nil {
		fmt.Println(err)
	}
	str := string(input)
	a := strings.Split(str, ",")

	var arr []int64
	for _, i := range a {
		if x, err := strconv.ParseInt(i, 0, 64); err == nil {
			arr = append(arr, x)
		}
	}
	fmt.Println("array: ", arr) // >> debug
	arr[1] = 12
	arr[2] = 2
	fmt.Println("array [modified]: ", arr) // >> debug
	for i := 0; i < len(arr); i += 4 {
		var number_a, number_b, num_c int64
		operator := arr[i]
		// if i+1 == 1 {
		// 	arr[1] = 12
		// }
		// if i+2 == 2 {
		// 	arr[2] = 2
		// }
		if (i + 1) <= len(arr) {
			number_a = arr[i+1]
		}

		if (i + 2) <= len(arr) {
			number_a = arr[i+2]
		}

		if (i + 3) <= len(arr) {
			num_c = arr[i+3]
		}

		fmt.Println("operator: ", operator, "number_a: ", number_a, "number_b: ", number_b, "num_c: ", num_c) // >> debug
		if operator == 99 {
			fmt.Println(arr)
			break
		} else if operator == 1 {
			arr[num_c] = arr[number_a] + arr[number_b]
			fmt.Println("operator ==> ", operator, " position of result", num_c, " result: ", arr[number_a]+arr[number_b])
		} else if operator == 2 {
			arr[num_c] = arr[number_a] * arr[number_b]
			fmt.Println("operator ==> ", operator, " positionof result ", num_c, " result: ", arr[number_a]*arr[number_b])
		}
	}
}
func main() {
	part_1()
	// part1_sample()
}
